Sequence Alignment and Analysis of the astrocytal gene expression data 
======================================================================

Acquiring the gene expression data
----------------------------------

The set of data pertaining to gene expression in brain cells of an astrocytal type (injected with dexamethasone) was obtained. The script presented immediately below was used.

::

       #!/bin/bash
       scp User@DataServer:/results/analysis/output/Home/Astro_dex_1_DataProvider_084/basecaller_results/IonXpressRNA_00[1-2]_rawlib.basecaller.bam ./astro_dex_1
       scp User@DataServer:/results/analysis/output/Home/Astro_dex_2_DataProvider_088/basecaller_results/IonXpressRNA_00[1-2]_rawlib.basecaller.bam ./astro_dex_2
       scp User@DataServer:/results/analysis/output/Home/Astro_dex_3_DataProvider_090/basecaller_results/IonXpressRNA_00[1-2]_rawlib.basecaller.bam ./astro_dex_3
       scp User@DataServer:/results/analysis/output/Home/Astro_dex_4_DataProvider_091/basecaller_results/IonXpressRNA_00[3-4]_rawlib.basecaller.bam ./astro_dex_4
       scp User@DataServer:/results/analysis/output/Home/Astro_dex_5_DataProvider_094/basecaller_results/IonXpressRNA_00[3-4]_rawlib.basecaller.bam ./astro_dex_5

The obtained sequence data is being stored in binary files (BAM files). We convert the files to FASTA format, which makes the data within text-based.


::

       #!/bin/bash
       bam bam2FastQ --in ./astro_dex_1/IonXpressRNA_001_rawlib.basecaller.bam --outBase ./astro_dex_1/ctrl1
       bam bam2FastQ --in ./astro_dex_1/IonXpressRNA_002_rawlib.basecaller.bam --outBase ./astro_dex_1/dex1
       bam bam2FastQ --in ./astro_dex_2/IonXpressRNA_001_rawlib.basecaller.bam --outBase ./astro_dex_2/ctrl2
       bam bam2FastQ --in ./astro_dex_2/IonXpressRNA_002_rawlib.basecaller.bam --outBase ./astro_dex_2/dex2
       bam bam2FastQ --in ./astro_dex_3/IonXpressRNA_001_rawlib.basecaller.bam --outBase ./astro_dex_3/ctrl3
       bam bam2FastQ --in ./astro_dex_3/IonXpressRNA_002_rawlib.basecaller.bam --outBase ./astro_dex_3/dex3
       bam bam2FastQ --in ./astro_dex_4/IonXpressRNA_003_rawlib.basecaller.bam --outBase ./astro_dex_4/ctrl4
       bam bam2FastQ --in ./astro_dex_4/IonXpressRNA_004_rawlib.basecaller.bam --outBase ./astro_dex_4/dex4
       bam bam2FastQ --in ./astro_dex_5/IonXpressRNA_003_rawlib.basecaller.bam --outBase ./astro_dex_5/ctrl5
       bam bam2FastQ --in ./astro_dex_5/IonXpressRNA_004_rawlib.basecaller.bam --outBase ./astro_dex_5/dex5

In order to perform sequence aligning, a reference sequence is needed. In practice (we utilise Bowtie2 and Tophat2 tools in the process) it means that we need a fasta file containing a genome of species used in lab and a corresponding index file. In our case, the research was carried out with *Mus musculus* specimens as test subjects. Naturally, the house mouse genome was chosen as a comparison for our data. 


The genome was downloaded from `UCSC <http://genome.ucsc.edu/index.html>`_ (*University of California Santa Cruz*) database in the form of FASTA files containing specific chromosome sequences. Then, a Bowtie2 tool was used - bowtie2-build that uses separate chromosome fasta files to create a single index file for the whole house mouse genome.

The next step is to align our data to the genome with TopHat. Before that, however, we obtain a house mouse related GTF file from `UCSC Table Browser <http://genome.ucsc.edu/cgi-bin/hgTables?command=start>`_. The file describes sequence positions of known coding sequences as well as those of exons. It also contains gene and transcript annotations. Provided it is supplied to TopHat as a optional input file, TopHat's sequence aligning speeds up. Here is an excerpt from the file:
 

::


          chr1	mm10_ensGene	stop_codon	134202951	134202953	0.000000	-	.	gene_id "ENSMUST00000086465"; transcript_id "ENSMUST0000008
          chr1	mm10_ensGene	CDS	134202954	134203590	0.000000	-	1	gene_id "ENSMUST00000086465"; transcript_id "ENSMUST00000086465"; 
          chr1	mm10_ensGene	exon	134199223	134203590	0.000000	-	.	gene_id "ENSMUST00000086465"; transcript_id "ENSMUST00000086465"; 
          chr1	mm10_ensGene	CDS	134234015	134234355	0.000000	-	0	gene_id "ENSMUST00000086465"; transcript_id "ENSMUST00000086465"; 
          chr1	mm10_ensGene	start_codon	134234353	134234355	0.000000	-	.	gene_id "ENSMUST00000086465"; transcript_id "ENSMUST0000008
          chr1	mm10_ensGene	exon	134234015	134234412	0.000000	-	.	gene_id "ENSMUST00000086465"; transcript_id "ENSMUST00000086465";


It can be noticed that gene and transcript annotations are exactly the same and that should not be the case. The corrections has been made with the followingR script:

::


	setwd("DISK:/CHOSEN_DIRECTORY/")

	GTF <- read.table("mm10.gtf", stringsAsFactors = FALSE)
	Gene_Ids <- read.table("geneids", stringsAsFactors = FALSE)

	for (i in (1:nrow(GTF))) {
  
	   GTF[i,10] <- Gene_Ids[(match(x = GTF[i,13],table = Gene_Ids[,1])),2]
	}	


	write.table(GTF, file = "mm10_proper.gtf", quote = FALSE, sep = "\t", row.names = F, col.names = F)




..  ::


..       sed 's/ENSMUST/ENSMUSG/' mm10.gtf > mm10_full_geneids.gtf






.. The lines refering to exon sequences were removed with a command:

 
..  ::


..       sed '/exon/d' mm10_full_geneids.gtf > mm10_trsc.gtf 






and as a result we were left with the file as showcased below:

::


	chr1	mm10_ensGene	exon	3054233	3054733	0	+	.	gene_id	ENSMUSG00000090025	;	transcript_id	ENSMUST00000160944	;
	chr1	mm10_ensGene	exon	3102016	3102125	0	+	.	gene_id	ENSMUSG00000064842	;	transcript_id	ENSMUST00000082908	;
	chr1	mm10_ensGene	exon	3205901	3207317	0	-	.	gene_id	ENSMUSG00000051951	;	transcript_id	ENSMUST00000162897	;
	chr1	mm10_ensGene	exon	3206523	3207317	0	-	.	gene_id	ENSMUSG00000051951	;	transcript_id	ENSMUST00000159265	;
	chr1	mm10_ensGene	exon	3213439	3215632	0	-	.	gene_id	ENSMUSG00000051951	;	transcript_id	ENSMUST00000159265	;
	chr1	mm10_ensGene	exon	3213609	3216344	0	-	.	gene_id	ENSMUSG00000051951	;	transcript_id	ENSMUST00000162897	;




..  head -n 50000 dex1.fastq > first50000.fastq
..  paste - - - - < first50000.fastq > pasteout
..  awk 'length($2) > 149' pasteout > 150_and_longer_1000.fastq
..  tr "\t" "\n" < 150_and_longer_1000.fastq > ss
..  head -n 4000 ss > 150_and_longer_1000.fastq

 




